import React, { useState } from "react";
import "./Slider.css";

function Slider() {
  const [isDarkTheme, setIsDarkTheme] = useState(false);

  const changeTheme = () => {
    document.body.classList.toggle("dark");
    setIsDarkTheme(!isDarkTheme);
  };

  return (
    <label className="switch" htmlFor="checkbox">
      <input type="checkbox" id="checkbox" />
      <div
        className={`slider round ${isDarkTheme ? "dark" : ""}`}
        onClick={changeTheme}
      ></div>
    </label>
  );
}

export default Slider;
