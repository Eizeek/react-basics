import "./App.css";
import Slider from "./Slider";

function App() {
  return (
    <div className="App">
      <Slider />
      <h1 style={{ color: "#999", fontSize: "19px" }}>Solar system planets</h1>
      <ul className="planets-list">
        <li>Mercury</li>
        <li>Venus</li>
        <li>Earth</li>
        <li>Mars</li>
        <li>Jupiter</li>
        <li>Saturn</li>
        <li>Uranus</li>
        <li>Neptune</li>
      </ul>
    </div>
  );
}

export default App;
